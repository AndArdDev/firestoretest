package com.example.firestoretest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private EditText
            titleEditText,
            descriptionEditText,
            priorityEditText;

    private TextView dataTextView;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference noteBook = db.collection("NoteBook");

    private DocumentSnapshot lastSnapshot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleEditText = findViewById(R.id.title_edit_text);
        descriptionEditText = findViewById(R.id.description_edit_text);
        priorityEditText = findViewById(R.id.priority_edit_text);
        dataTextView = findViewById(R.id.data_text_view);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_button:
                String title, description;
                title = titleEditText.getText().toString();
                description = descriptionEditText.getText().toString();

                if (priorityEditText.length() == 0) {
                    priorityEditText.setText("0");
                }

                int priority = Integer.parseInt(priorityEditText.getText().toString());

                Note note = new Note(title, description, priority);

                noteBook.add(note);
                break;

            case R.id.load_button:
                Query query;
                if (lastSnapshot == null) {
                    query = noteBook.orderBy("priority")
                            .limit(3);
                } else {
                    query = noteBook.orderBy("priority")
                            .startAfter(lastSnapshot)
                            .limit(3);
                }


                query.get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                String data = "";

                                for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                                    Note retrievedNote = queryDocumentSnapshot.toObject(Note.class);
                                    retrievedNote.setDocumentId(queryDocumentSnapshot.getId());

                                    data += "ID : " + retrievedNote.getDocumentId() + '\n' +
                                            "Title" + retrievedNote.getTitle() + '\n' +
                                            "Description : " + retrievedNote.getDescription() + '\n' +
                                            "Priority : " + retrievedNote.getPriority() + "\n\n";
                                }

                                if (queryDocumentSnapshots.size() > 0) {
                                    data += "________________\n\n";
                                    dataTextView.append(data);

                                    lastSnapshot = queryDocumentSnapshots.getDocuments()
                                            .get(queryDocumentSnapshots.size() - 1);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: " + e.toString());
                            }
                        });
                break;
        }
    }
}